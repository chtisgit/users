package users

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

const sessionCookie = "SESSION"

// SessionFromHTTP looks at the SESSION cookie and tries to find an
// associated session.
func (m *UserManagement) SessionFromHTTP(r *http.Request) (*Session, error) {
	c, err := r.Cookie(sessionCookie)
	if err != nil {
		return nil, err
	}

	return m.Session(c.Value)
}

func (m *UserManagement) setCookie(w http.ResponseWriter, ss *Session) {
	http.SetCookie(w, &http.Cookie{
		Name:    sessionCookie,
		Path:    "/",
		Expires: time.Now().Add(time.Hour * 24 * 365),
		Value:   ss.Token,
	})
}

// StartSessionHTTP initiates a session for a given userID and sets the appropriate
// cookie in the ResponseWriter.
func (m *UserManagement) StartSessionHTTP(userID int32, w http.ResponseWriter) (*Session, error) {
	ss, err := m.StartSession(userID)
	if err == nil {
		logrus.WithField("token", ss.Token).Info("new session")
		m.setCookie(w, ss)
	} else {
		logrus.WithError(err).Error("start session")
	}
	return ss, err
}

// LoginHTTP handles a user login and writes the session cookie to the http.ResponseWriter
// if successful.
func (m *UserManagement) LoginHTTP(username, pass string, w http.ResponseWriter) (*Session, error) {
	ss, err := m.LoginUser(username, pass)
	if err == nil {
		m.setCookie(w, ss)
	}
	return ss, err
}

// LogoutHTTP looks up the session from the SESSION cookie and invalidates the session.
func (m *UserManagement) LogoutHTTP(r *http.Request) error {
	c, err := r.Cookie(sessionCookie)
	if err != nil {
		return nil
	}

	return m.InvalidateSession(c.Value)
}
