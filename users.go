package users

import (
	"database/sql"
	"errors"
	"time"

	"github.com/go-sql-driver/mysql"
)

// Handlers defines handlers for the UserManagement object.
type Handlers struct {
	Check       func(passHash, pass string) bool
	CreateToken func() string
	Expires     func() time.Time
}

// Scheme configures the UserManagement object.
type Scheme struct {
	UserTable    string
	SessionTable string

	Handle *Handlers
}

type queries struct {
	LoadUserQuery    string
	LoginUserQuery   string
	LogoutAllQuery   string
	LogoutUserQuery  string
	SetPasswordQuery string
	NewUserQuery     string
	SessionQuery     string
	DeleteUserQuery  string
}

// UserManagement is the interface to use for session and user management.
type UserManagement struct {
	db     *sql.DB
	q      queries
	handle *Handlers
}

// User holds the credentials and unique ID of a user.
type User struct {
	ID   int32
	Name string
	Pass string
}

// Session represents a logged in user's session.
type Session struct {
	UserID   int32
	Username string
	Expires  time.Time
	Token    string
}

// New creates a UserManagement instance from an sql.DB and a Scheme.
func New(db *sql.DB, sch *Scheme) *UserManagement {
	return &UserManagement{
		db:     db,
		handle: sch.Handle,
		q: queries{
			LoadUserQuery:    "SELECT id,pass FROM " + sch.UserTable + " WHERE name=?",
			LoginUserQuery:   "INSERT INTO " + sch.SessionTable + " (sess, expires, user) VALUES (?,?,?)",
			LogoutAllQuery:   "DELETE FROM " + sch.SessionTable,
			LogoutUserQuery:  "DELETE FROM " + sch.SessionTable + " WHERE sess=?",
			SetPasswordQuery: "UPDATE " + sch.UserTable + " SET pass=? WHERE name=?",
			NewUserQuery:     "INSERT INTO " + sch.UserTable + " (name,pass) VALUES (?,?)",
			SessionQuery:     "SELECT u.id,COALESCE(u.name,\"\"),s.sess,s.expires FROM " + sch.SessionTable + " AS s JOIN " + sch.UserTable + " AS u ON s.user=u.id AND s.sess=?",
			DeleteUserQuery:  "DELETE FROM " + sch.UserTable + " WHERE userId=?",
		},
	}
}

// CleanUser overwrites the password in the User struct.
func (m *UserManagement) CleanUser(u *User) {
	b := []byte(u.Pass)
	for i := range b {
		b[i] = 0
	}
}

func (m *UserManagement) loadUser(username string) (*User, error) {
	stmt, err := m.db.Prepare(m.q.LoadUserQuery)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	u := &User{Name: username}
	err = stmt.QueryRow(username).Scan(&u.ID, &u.Pass)

	return u, err
}

// ErrCheckFailed is returned by LoginUser when the Check handler returns false.
var ErrCheckFailed = errors.New("check failed")

// ErrSessionExpired may be returned by Session.
var ErrSessionExpired = errors.New("session expired")

// StartSession opens a new session for the given userID. The session object that
// is returned will not contain the user name.
func (m *UserManagement) StartSession(userID int32) (*Session, error) {
	stmt, err := m.db.Prepare(m.q.LoginUserQuery)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	ss := &Session{
		UserID:  userID,
		Expires: m.handle.Expires(),
		Token:   m.handle.CreateToken(),
	}

	_, err = stmt.Exec(ss.Token, ss.Expires, ss.UserID)
	return ss, err
}

// LoginUser handles user login. It takes a username and a password.
// If a user with these credentials exists, a new Session for it is created in the
// Database and the Session is returned. Otherwise the function returns an error.
// The handler Check is used to determine whether the password argument matches
// the password hash saved in the database.
// The handler CreateToken is used to create a session token if authentication
// succeeds.
func (m *UserManagement) LoginUser(username, pass string) (*Session, error) {
	u, err := m.loadUser(username)
	if err != nil {
		return nil, err
	}
	defer m.CleanUser(u)

	if !m.handle.Check(u.Pass, pass) {
		return nil, ErrCheckFailed
	}

	ss, err := m.StartSession(u.ID)
	ss.Username = u.Name

	return ss, err
}

// SetPassword saves a new password hash for a user.
func (m *UserManagement) SetPassword(username, passHash string) error {
	stmt, err := m.db.Prepare(m.q.SetPasswordQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(passHash, username)
	return err
}

// NewUser creates a new user.
func (m *UserManagement) NewUser(username, passHash string) (int32, error) {
	stmt, err := m.db.Prepare(m.q.NewUserQuery)
	if err != nil {
		return 0, err
	}
	defer stmt.Close()

	var info sql.Result
	if username == "" {
		info, err = stmt.Exec(nil, "")
	} else {
		info, err = stmt.Exec(username, passHash)
	}
	if err != nil {
		return 0, err
	}

	i, err := info.LastInsertId()
	return int32(i), err
}

// DeleteUser deletes a user.
func (m *UserManagement) DeleteUser(id int32) error {
	stmt, err := m.db.Prepare(m.q.DeleteUserQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	return err
}

// DeleteNullUser deletes a user only if name is null.
func (m *UserManagement) DeleteNullUser(id int32) error {
	stmt, err := m.db.Prepare(m.q.DeleteUserQuery + " AND name IS NULL")
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(id)
	return err
}

// Session searches a session that matches the token parameter and returns
// it if there is one. Otherwise it returns an error. If an expired session is
// found it is deleted using InvalidateSession and then ErrSessionExpired
// is returned.
func (m *UserManagement) Session(token string) (*Session, error) {
	stmt, err := m.db.Prepare(m.q.SessionQuery)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	ss := new(Session)

	var t mysql.NullTime
	err = stmt.QueryRow(token).Scan(&ss.UserID, &ss.Username, &ss.Token, &t)
	if !t.Valid {
		return nil, errors.New("not found")
	}
	ss.Expires = t.Time

	if ss.Expires.Before(time.Now()) {
		m.InvalidateSession(token)
		return nil, ErrSessionExpired
	}

	return ss, err
}

// InvalidateSession removes the session token from the session table.
func (m *UserManagement) InvalidateSession(token string) error {
	stmt, err := m.db.Prepare(m.q.LogoutUserQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(token)
	return err
}

// InvalidateAllSessions removes all entries from the session table which
// means all users are logged out.
func (m *UserManagement) InvalidateAllSessions() error {
	stmt, err := m.db.Prepare(m.q.LogoutAllQuery)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	return err
}
